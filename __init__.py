# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool

from . import account, inventory, invoice, product, shipment, stock


def register():
    Pool.register(
        account.FiscalYear,
        account.Configuration,
        account.ConfigurationStockJournal,
        account.AccountStockCostRule,
        account.AccountCategoryRules,
        account.Move,
        invoice.InvoiceLine,
        invoice.PurchaseLine,
        invoice.Invoice,
        inventory.Inventory,
        stock.Move,
        product.Category,
        product.CategoryAccount,
        product.Template,
        product.Product,
        shipment.ShipmentInternal,
        shipment.CreateMoveFromStockStart,
        module='account_stock_latin', type_='model')
    Pool.register(
        shipment.CreateMoveFromStock,
        inventory.InventoryCreateAccountMove,
        module='account_stock_latin', type_='wizard')
