from decimal import Decimal

from trytond.exceptions import UserError
from trytond.i18n import gettext
from trytond.model import ModelView, fields
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction
from trytond.wizard import Button, StateTransition, StateView, Wizard

from .exceptions import NotificationError


class ShipmentInternal(metaclass=PoolMeta):
    "Internal Shipment"
    __name__ = 'stock.shipment.internal'

    account_move = fields.Many2One('account.move', 'Account Move', readonly=True)

    @property
    def rule_pattern(self):
        return {
            'from_location': self.from_location.id,
            'to_location': self.to_location.id,
            }

    @classmethod
    def done(cls, shipments):
        super(ShipmentInternal, cls).done(shipments)
        cls.create_account_move(shipments)

    @classmethod
    def create_account_move(cls, shipments):
        print('ingresa a create account move')
        pool = Pool()
        Rule = pool.get('account.stock.cost.rule')
        Move = pool.get('account.move')
        rules = Rule.search([])
        moves_to_create = []
        for shipment in shipments:
            if shipment.account_move:
                raise NotificationError(gettext('account_stock_latin.msg_notification_move'))
            pattern = shipment.rule_pattern
            for rule in rules:
                if rule.match(pattern):
                    move = cls.get_account_move(shipment, rule)
                    if move:
                        moves_to_create.append(move)
                    break
                else:
                    continue
        if moves_to_create:
            moves = Move.create(moves_to_create)
            Move.post(moves)
            for mv in moves:
                shipment = mv.origin
                shipment.account_move = mv
                shipment.save()

    @classmethod
    def get_account_move(cls, shipment, rule):
        pool = Pool()
        Uom = pool.get('product.uom')
        Configuration = pool.get('account.configuration')
        Period = pool.get('account.period')

        configuration = Configuration(1)
        if not configuration.stock_journal:
            raise UserError(gettext(
                'account_stock_latin.msg_missing_journal_stock_configuration',
            ))
        journal = configuration.stock_journal.id
        except_period = Transaction().context.get('except_period')
        period_id = None
        if not except_period:
            period_id = Period.find(shipment.company.id, date=shipment.effective_date).id

        lines_to_create = []
        moves_error = []
        for move in shipment.outgoing_moves:
            if move.quantity == 0:
                continue
            prd = move.product
            cat = prd.account_category.id
            account_debit = prd.account_expense_used
            for r in rule.account_rules:
                if r.category.id == cat:
                    account_debit = r.account
            party = shipment.company if account_debit.party_required else None

            cost_price = Uom.compute_price(prd.default_uom,
                prd.cost_price, move.unit)
            amount = shipment.company.currency.round(
                Decimal(str(move.quantity)) * cost_price)
            line_debit = {
                'account': account_debit.id,
                'party': party.id,
                'debit': amount,
                'credit': Decimal(0),
                'description': prd.name,
            }
            op = bool(hasattr(shipment, "operation_center"))
            if op:
                line_debit.update({'operation_center': shipment.operation_center.id})

            if shipment.analytic_account and account_debit.type.statement != 'balance':
                line_analytic = {
                    'account': shipment.analytic_account.id,
                    'debit': amount,
                    'credit': Decimal(0),
                    }
                line_debit['analytic_lines'] = [
                    ('create', [line_analytic])]
            lines_to_create.append(line_debit)
            account_credit = prd.account_category.account_stock
            if prd.account_category.account_stock_out:
                account_credit = prd.account_category.account_stock_out
            if not account_credit:
                raise UserError(gettext(
                    'account_stock_latin.msg_missing_account_stock',
                    product=prd.name,
                ))
            party = shipment.company if account_credit.party_required else None

            line_credit = {
                'account': account_credit.id,
                'party': party.id,
                'debit': Decimal(0),
                'credit': amount,
                'description': prd.name,
            }
            lines_to_create.append(line_credit)
        if moves_error:
            return None
        account_move = {
            'journal': journal,
            'date': shipment.effective_date,
            'origin': str(shipment),
            'company': shipment.company,
            'period': period_id,
            'lines': [('create', lines_to_create)],
        }
        return account_move


class CreateMoveFromStockStart(ModelView):
    "Create Move From Stock Start"
    __name__ = 'account_stock_latin.create_move.start'
    company = fields.Many2One('company.company', 'Company', required=True)
    period = fields.Many2One('account.period', 'Period', required=True)
    move_date = fields.Date('Move Date', required=True)
    start_date = fields.Date('Start Date', required=True)
    end_date = fields.Date('End Date', required=True)

    @staticmethod
    def default_company():
        return Transaction().context.get('company')


class CreateMoveFromStock(Wizard):
    "Create Move From Stock"
    __name__ = 'account_stock_latin.create_move'
    start = StateView('account_stock_latin.create_move.start',
        'account_stock_latin.create_move_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Create Move', 'create_move', 'tryton-ok', default=True),
        ])
    create_move = StateTransition()

    def transition_create_move(self):
        pool = Pool()
        ShipmentInternal = pool.get('stock.shipment.internal')
        shipments = ShipmentInternal.search(
            [
                ('account_move', '=', None),
                ('state', '=', 'done'),
                ('effective_date', '>=', self.start.start_date),
                ('effective_date', '<=', self.start.end_date),
            ],
        )
        Rule = pool.get('account.stock.cost.rule')
        Move = pool.get('account.move')
        rules = Rule.search([])
        moves_to_create = []
        period_id = self.start.period.id
        move_date = self.start.move_date
        with Transaction().set_context({'except_period': True}):
            for shipment in shipments:
                pattern = shipment.rule_pattern
                for rule in rules:
                    if rule.match(pattern):
                        move = ShipmentInternal.get_account_move(shipment)
                        if move:
                            move['period'] = period_id
                            move['date'] = move_date
                            moves_to_create.append(move)
                        break
                    else:
                        continue
        if moves_to_create:
            moves = Move.create(moves_to_create)
            Move.post(moves)
            for mv in moves:
                shipment = mv.origin
                shipment.account_move = mv
                shipment.save()

        return 'end'
