# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pyson import Eval
from trytond.pool import PoolMeta, Pool

from trytond.modules.account_product.product import (
    account_used, template_property)

account_names = ['account_stock', 'account_cogs',
                 'account_stock_in', 'account_stock_out']


class Category(metaclass=PoolMeta):
    __name__ = 'product.category'
    account_cogs = fields.MultiValue(fields.Many2One(
            'account.account',
            'Account Cost of Goods Sold', domain=[
                 ('closed', '!=', True),
                 ('type.expense',
                  '=', True),
                 ('company', '=', Eval(
                     'context', {}).get('company', -1)),
                 ],
            states={
                 'invisible': (~Eval('context', {}, ).get('company')
                               | Eval('account_parent')
                               | ~Eval('accounting', False)),
                 },
            depends=['account_parent', 'accounting']))
    account_stock = fields.MultiValue(fields.Many2One(
            'account.account', "Account Stock",
            domain=[
                ('closed', '!=', True),
                ('type.stock', '=', True),
                ('company', '=', Eval('context', {}).get('company', -1)),
            ],
            states={
                'invisible': (~Eval('context', {}).get('company')
                              | Eval('account_parent')
                              | ~Eval('accounting', False)),
                },
            depends=['account_parent', 'accounting']))
    account_stock_in = fields.MultiValue(fields.Many2One(
            'account.account', "Account Stock IN",
            domain=[
                ('closed', '!=', True),
                ('type.stock', '=', True),
                ('id', '!=', Eval('account_stock', -1)),
                ('company', '=', Eval('context', {}).get('company', -1)),
                ],
            states={
                'invisible': (~Eval('context', {}).get('company')
                              | Eval('account_parent')
                              | ~Eval('accounting', False)),
                },
            depends=['account_parent', 'accounting', 'account_stock']))
    account_stock_out = fields.MultiValue(fields.Many2One(
            'account.account', "Account Stock OUT",
            domain=[
                ('closed', '!=', True),
                ('type.stock', '=', True),
                ('id', '!=', Eval('account_stock', -1)),
                ('company', '=', Eval('context', {}).get('company', -1)),
                ],
            states={
                'invisible': (~Eval('context', {}).get('company')
                              | Eval('account_parent')
                              | ~Eval('accounting', False)),
                },
            depends=['account_parent', 'accounting', 'account_stock']))

    @classmethod
    def multivalue_model(cls, field):
        pool = Pool()
        if field in account_names:
            return pool.get('product.category.account')
        return super(Category, cls).multivalue_model(field)

    @property
    @account_used('account_cogs')
    def account_cogs_used(self):
        pass

    @property
    @account_used('account_stock')
    def account_stock_used(self):
        pass

    @property
    @account_used('account_stock_in')
    def account_stock_in_used(self):
        pass

    @property
    @account_used('account_stock_out')
    def account_stock_out_used(self):
        pass

    @fields.depends(
        'accounting', 'account_stock', 'account_stock_in', 'account_stock_out')
    def on_change_accounting(self):
        super().on_change_accounting()
        if not self.accounting:
            self.account_cogs = None
            self.account_stock = None
            self.account_stock_in = None
            self.account_stock_out = None


class CategoryAccount(metaclass=PoolMeta):
    __name__ = 'product.category.account'
    account_cogs = fields.Many2One(
        'account.account', "Account Cost of Goods Sold",
        domain=[
            ('closed', '!=', True),
            ('type.expense', '=', True),
            ('company', '=', Eval('company', -1)),
        ],
        depends=['company'])
    account_stock = fields.Many2One(
        'account.account', "Account Stock",
        domain=[
            ('closed', '!=', True),
            ('type.stock', '=', True),
            ('type.statement', '=', 'balance'),
            ('company', '=', Eval('company', -1)),
        ],
        depends=['company'])

    account_stock_in = fields.Many2One(
        'account.account', "Account Stock IN",
        domain=[
            ('closed', '!=', True),
            ('type.stock', '=', True),
            ('type.statement', '=', 'balance'),
            ('company', '=', Eval('company', -1)),
            ],
        depends=['company'])
    account_stock_out = fields.Many2One(
        'account.account', "Account Stock OUT",
        domain=[
            ('closed', '!=', True),
            ('type.stock', '=', True),
            ('type.statement', '=', 'balance'),
            ('company', '=', Eval('company', -1)),
            ],
        depends=['company'])


class Template(metaclass=PoolMeta):
    __name__ = 'product.template'

    @property
    @account_used('account_cogs', 'account_category')
    def account_cogs_used(self):
        pass

    @property
    @account_used('account_stock', 'account_category')
    def account_stock_used(self):
        pass

    @property
    @account_used('account_stock_in', 'account_category')
    def account_stock_in_used(self):
        pass

    @property
    @account_used('account_stock_out', 'account_category')
    def account_stock_out_used(self):
        pass


class Product(metaclass=PoolMeta):
    __name__ = 'product.product'
    account_cogs_used = fields.Function(fields.Many2One(
        'account.account', "Account COGS"), 'get_account_stock_category')
    account_stock_used = fields.Function(fields.Many2One(
        'account.account', "Account Stock"), 'get_account_stock_category')
    account_stock_in_used = template_property('account_stock_in_used')
    account_stock_out_used = template_property('account_stock_out_used')

    def get_account_stock_category(self, name):
        if self.account_category:
            if name == 'account_cogs_used' and self.account_category.account_cogs:
                return self.account_category.account_cogs.id
            if name == 'account_stock_used' and self.account_category.account_stock:
                return self.account_category.account_stock.id
