# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import MatchMixin, ModelSQL, ModelView, ValueMixin, fields
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval
from trytond.transaction import Transaction

stock_journal = fields.Many2One(
    'account.journal', "Stock Journal", required=True)


class Configuration(metaclass=PoolMeta):
    __name__ = 'account.configuration'
    stock_journal = fields.MultiValue(stock_journal)

    @classmethod
    def default_stock_journal(cls, **pattern):
        return cls.multivalue_model('stock_journal').default_stock_journal()


class ConfigurationStockJournal(ModelSQL, ValueMixin):
    "Account Configuration Stock Journal"
    __name__ = 'account.configuration.stock_journal'
    stock_journal = stock_journal

    @classmethod
    def default_stock_journal(cls):
        ModelData = Pool().get('ir.model.data')
        try:
            return ModelData.get_id('account', 'journal_stock')
        except KeyError:
            return None


class FiscalYear(metaclass=PoolMeta):
    __name__ = 'account.fiscalyear'
    account_stock_method = fields.Selection([
            (None, 'None'),
            ('latin', 'Latin America'),
        ], 'Account Stock Method')


class Move(metaclass=PoolMeta):
    __name__ = 'account.move'

    @classmethod
    def _get_origin(cls):
        origins = super(Move, cls)._get_origin()
        return [*origins, 'stock.shipment.internal', 'stock.inventory']


class AccountStockCostRule(MatchMixin, ModelSQL, ModelView):
    "Account Stock Cost Rule"

    __name__ = 'account.stock.cost.rule'

    company = fields.Many2One('company.company', 'Company', required=True)
    from_location = fields.Many2One('stock.location', 'From Location', required=True)
    to_location = fields.Many2One('stock.location', 'To Location', required=True)
    account_rules = fields.One2Many('account.category.rule', 'location_rule', 'Account Category Rules')

    @staticmethod
    def default_company():
        return Transaction().context.get('company')


class AccountCategoryRules(ModelSQL, ModelView):
    "Account Category Rules"
    __name__ = 'account.category.rule'

    location_rule = fields.Many2One('account.stock.cost.rule', 'Rule Stock')
    account = fields.Many2One('account.account',
        'Account', domain=[
            ('company', '=', Eval('context', {}).get('company', -1)),
            ('closed', '!=', True),
            ('type.expense', '=', True),
        ])
    category = fields.Many2One('product.category', 'Category', domain=[('accounting', '=', True)])
