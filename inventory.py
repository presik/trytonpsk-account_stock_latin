# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from decimal import Decimal

from trytond.exceptions import UserError
from trytond.i18n import gettext
from trytond.model import fields
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction
from trytond.wizard import StateTransition, Wizard


class Inventory(metaclass=PoolMeta):
    __name__ = 'stock.inventory'

    account_move = fields.Many2One('account.move', 'Move', readonly=True)

    def create_account_move(self):
        pool = Pool()
        Move = pool.get('account.move')
        if not self.account_move:
            move = self.get_account_move()
            moves = Move.create([move])
            Move.post(moves)
            self.account_move = moves[0]
            self.save()

    def get_account_move(self):
        pool = Pool()
        Uom = pool.get('product.uom')
        Configuration = pool.get('account.configuration')
        Period = pool.get('account.period')

        configuration = Configuration(1)
        if not configuration.stock_journal:
            raise UserError(gettext(
                'account_stock_latin.msg_missing_journal_stock_configuration',
            ))
        journal = configuration.stock_journal.id
        except_period = Transaction().context.get('except_period')
        period_id = None
        if not except_period:
            period_id = Period.find(self.company.id, date=self.date).id

        lines_to_create = []
        for line in self.lines:
            diff_quantity = line.difference_quantity
            if diff_quantity == 0:
                continue
            prd = line.product
            account_expense = prd.account_expense_used
            account_stock = prd.account_category.account_stock
            if diff_quantity > 0:
                account_credit = account_expense
                account_debit = account_stock
            else:
                account_credit = account_stock
                account_debit = account_expense

            party = self.company if account_debit.party_required else None
            if not account_credit:
                raise UserError(gettext(
                    'account_stock_latin.msg_missing_account_stock',
                    product=prd.name,
                ))

            with Transaction().set_context(stock_date_end=self.date):
                cost_price = prd.avg_cost_price

            cost_price = Uom.compute_price(prd.default_uom, cost_price, line.unit)

            amount = abs(self.company.currency.round(
                Decimal(str(diff_quantity)) * cost_price))
            line_debit = {
                'account': account_debit.id,
                'party': party.id,
                'debit': amount,
                'credit': Decimal(0),
                'description': prd.name,
            }

            party = self.company if account_credit.party_required else None

            line_credit = {
                'account': account_credit.id,
                'party': party.id,
                'debit': Decimal(0),
                'credit': amount,
                'description': prd.name,
            }
            op = bool(hasattr(self, "operation_center"))
            if op and self.operation_center:
                line_debit.update({'operation_center': self.operation_center.id})
                line_credit.update({'operation_center': self.operation_center.id})

            analytic_account = bool(hasattr(self, "analytic_account"))
            if analytic_account and self.analytic_account:
                if account_debit.type.statement != 'balance':
                    line_analytic = {
                        'account': self.analytic_account.id,
                        'debit': amount,
                        'credit': Decimal(0),
                        }
                    line_debit['analytic_lines'] = [('create', [line_analytic])]
                if account_credit.type.statement != 'balance':
                    line_analytic = {
                        'account': self.analytic_account.id,
                        'debit': Decimal(0),
                        'credit': amount,
                        }
                    line_credit['analytic_lines'] = [('create', [line_analytic])]
            lines_to_create.append(line_debit)
            lines_to_create.append(line_credit)
        account_move = {
            'journal': journal,
            'date': self.date,
            'origin': str(self),
            'company': self.company,
            'period': period_id,
            'lines': [('create', lines_to_create)],
        }
        return account_move


class InventoryCreateAccountMove(Wizard):
    "Inventory Create Account Move"
    __name__ = 'stock.inventory.create_account_move'
    start_state = 'create_account_move'
    create_account_move = StateTransition()

    def transition_create_account_move(self):
        Inventory = Pool().get('stock.inventory')
        inventory_id = Transaction().context['active_id']

        if not inventory_id:
            return 'end'
        inventory = Inventory(inventory_id)
        inventory.create_account_move()
        return 'end'
