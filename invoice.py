# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import operator
from decimal import Decimal

from trytond.exceptions import UserError
from trytond.i18n import gettext
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction


class PurchaseLine(metaclass=PoolMeta):
    __name__ = 'purchase.line'

    def get_invoice_line(self):
        records = super(PurchaseLine, self).get_invoice_line()
        for rec in records:
            account_stock_used = self.product.account_stock_used
            if self.product.template.type == 'goods' and account_stock_used:
                rec.account = account_stock_used.id
        return records


class Invoice(metaclass=PoolMeta):
    __name__ = 'account.invoice'

    @classmethod
    def __setup__(cls):
        super(Invoice, cls).__setup__()
        cls._buttons.update({
                'solve_move_latin': {
                    },
                })

    @classmethod
    def solve_move_latin(cls, records):
        MoveLine = Pool().get('account.move.line')
        cursor = Transaction().connection.cursor()

        query = """
            SELECT DISTINCT i.id AS invoice
            FROM account_invoice AS i
            JOIN account_move AS m ON i.move = m.id
            JOIN account_move_line AS ml ON ml.move = m.id
            JOIN account_account AS a ON ml.account = a.id
            JOIN account_invoice_line AS il ON i.id = il.invoice
            JOIN product_product AS p ON il.product = p.id
            JOIN product_template AS t ON p.template = t.id
            WHERE a.code LIKE '13%' 
            AND a.type IS NOT NULL
            AND t.type = 'goods'
            AND i.state in ('posted', 'paid')
            AND NOT EXISTS (
                SELECT 1
                FROM account_move_line AS ml2
                JOIN account_account AS a2 ON ml2.account = a2.id
                WHERE ml2.move = m.id
                AND (a2.code LIKE '14%' 
                OR a2.code LIKE '17%')
            ) order by i.id desc limit 1;
        """
        cursor.execute(query)
        result = cursor.fetchall()
        invoice_ids = [m[0] for m in result]
        invoices = cls.browse(invoice_ids)
        for inv in invoices:
            if inv.type == 'out' and inv.state in ('posted', 'paid'):
                to_create = []
                for line in inv.lines:
                    move_lines = line.get_move_lines()
                    if move_lines:
                        for ln in move_lines:
                            if line.account.id != ln.account.id:
                                ln.move = inv.move.id
                                to_create.append(ln)
                if to_create:
                    cursor.execute("update account_move set state='draft' WHERE id = %s", (inv.move.id,))
                    MoveLine.save(to_create)
                    cursor.execute("update account_move set state='posted' WHERE id = %s", (inv.move.id,))


class InvoiceLine(metaclass=PoolMeta):
    __name__ = 'account.invoice.line'

    @classmethod
    def __setup__(cls):
        super(InvoiceLine, cls).__setup__()

    # def _account_domain(type_):
    #     # This method overload original adding stock type to "in" domain
    #     if type_ == 'out':
    #         return ['OR', ('type.revenue', '=', True)]
    #     elif type_ == 'in':
    #         return ['OR',
    #                 ('type.expense', '=', True),
    #                 ('type.debt', '=', True),
    #                 ('type.stock', '=', True),
    #                 ]

    def _get_latin_move_lines(self, amount, type_, account_stock_method):
        """
        Return account move for latin stock accounting
        """
        self.amount = abs(self.amount)
        MoveLine = Pool().get('account.move.line')

        assert type_.startswith('in_') or type_.startswith('out_'), \
            'wrong type'
        result = []
        move_line = MoveLine()
        desc = self.product.name if self.product else self.description
        move_line.description = desc
        move_line.amount_second_currency = None
        move_line.second_currency = None
        account_type = 'account_stock'
        if type_ == 'in_customer':
            move_line.debit = amount
            move_line.credit = Decimal('0.0')
        else:
            move_line.debit = Decimal('0.0')
            move_line.credit = amount
        account = getattr(self.product.account_category, account_type)
        account_stock_out = self.product.account_category.account_stock_out
        if account_stock_out:
            account = account_stock_out
        move_line.account = account
        if not move_line.account:
            raise UserError(gettext(
                'account_stock_latin.msg_missing_account_stock',
                product=self.product.name,
            ))
        if move_line.account.party_required:
            move_line.party = self.invoice.party

        result.append(move_line)
        debit, credit = move_line.debit, move_line.credit

        move_line = MoveLine()

        move_line.description = self.description
        move_line.amount_second_currency = move_line.second_currency = None
        move_line.debit, move_line.credit = credit, debit

        move_line.account = self.product.account_cogs_used
        if not move_line.account:
            raise UserError(gettext(
                'account_stock_latin.msg_missing_account_cogs_used',
                product=self.product.name,
            ))
        if move_line.account.party_required:
            move_line.party = self.invoice.party

        result.append(move_line)
        return result

    # def _temporal_fix(self, result):
    #     # Change account expense by stock based in category, just to 31-12-2021
    #     for ln in result:
    #         acc = self.product.account_stock_used
    #         if self.quantity < 0 and self.product.account_category.account_return_purchase:
    #             acc = self.product.account_category.account_return_purchase
    #         if acc and ln.account != acc:
    #             ln.account = acc
    #     return result

    def get_move_lines(self):
        pool = Pool()
        Move = pool.get('stock.move')
        Period = pool.get('account.period')
        invoice = self.invoice
        result = super(InvoiceLine, self).get_move_lines()
        product = self.product
        if self.type != 'line' or not product or product.type != 'goods' or product.consumable is True:
            return result

        accounting_date = (invoice.accounting_date or invoice.invoice_date)
        period_id = Period.find(invoice.company.id, date=accounting_date)
        period = Period(period_id)
        account_stock_method = period.fiscalyear.account_stock_method
        print('invoice.account_stock_method', account_stock_method)
        if account_stock_method != 'latin':
            return result

        if invoice.type == 'in':
            # result = self._temporal_fix(result)
            return result

        moves = []
        # for move in self.stock_moves:
        #     if move.state != 'done':
        #         continue
        #
        #     # remove move for different product
        #     if move.product != product:
        #         raise UserWarning(gettext(
        #             'account_stock_latin.msg_invoice_line_stock_move_different_product'
        #         ))
        #     else:
        #         moves.append(move)

        type_ = 'out_customer'
        if self.quantity < 0:
            direction, target = type_.split('_')
            direction = 'out' if direction == 'in' else 'in'
            type_ = f'{direction}_{target}'

        moves.sort(key=operator.attrgetter('effective_date'))
        cost = Move.update_latin_quantity_product_cost(
            product, moves, abs(self.quantity), self.unit, type_)
        cost = invoice.currency.round(cost)

        with Transaction().set_context(date=accounting_date):
            latin_move_lines = self._get_latin_move_lines(
                cost, type_, account_stock_method)
            print('latin_move_lines', latin_move_lines)
        result.extend(latin_move_lines)
        return result
